# หุ้นหรรษา #

### Root access ###

* User: deknoi
* Pass: deknoi

### โจทย์ ###

* สร้างกราฟ Candlestick จากหุ้นในฐานข้อมูลที่มีให้ ( หุ้นชื่อ SUPER )
* ข้อมูลราคาจะอยู่ในฐานข้อมูลโมเดลชื่อ Quotation โดยจะเก็บเป็น ราคาเปิด, ราคาสูงสุดในวันนั้น, ราคาต่ำสุดในวันนั้น, ราคาปิด และข้อมูลวันที่ ( สามารถเข้าไปดูข้อมูลได้ที่ /admin ภายใน django ครับ )

* ข้อมูลกราฟประเภท Candlestick http://en.wikipedia.org/wiki/Candlestick_chart

### Output ###

* ![chart.png](https://bitbucket.org/repo/dAR9rr/images/782010149-chart.png)


### ตัวช่วย ###
* Timestamp สามารถใช้ property create_timestamp ได้เลย
* หากไม่มั่นใจว่าข้อมูลฝั่ง View เขียนมาถูกไหม สามารถดึงข้อมูลมาจาก static/highstock/test.json มาเทสก่อนได้ครับ (format ข้อมูลนำมาจาก demo ของ HighStock)

### Getting Started ###
* Python : http://www.codecademy.com/tracks/python
* Django : https://docs.djangoproject.com/en/1.6/intro/tutorial01/
* jQuery : http://www.codecademy.com/tracks/jquery
* HighStock (Demo) : http://www.highcharts.com/stock/demo/candlestick-and-volume
* HighStock : http://api.highcharts.com/highstock