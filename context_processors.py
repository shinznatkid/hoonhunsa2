#!/usr/bin/python
# -*- coding: utf-8

from django.conf import settings

def context(request):
    context = {}
    if 'SITENAME' not in request:
        context['SITENAME'] = getattr(settings,'SITENAME','')

    return context