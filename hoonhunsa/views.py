# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect, get_object_or_404, HttpResponse
from django.template import RequestContext

def index(request):
	data = {}
	return render_to_response('index.html', data, context_instance=RequestContext(request))