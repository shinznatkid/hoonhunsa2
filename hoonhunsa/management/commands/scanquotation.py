#!/usr/bin/python
# -*- coding: utf-8

from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from datetime import datetime as dt
from django.conf import settings
from optparse import make_option
from hoonhunsa.models import *
from threading import Thread
from pytz import timezone
from Queue import Queue
import requests
import datetime
import sys

class Command(BaseCommand):
	option_list = BaseCommand.option_list + (
		make_option(
			'--scanall',
			action='store_true',
			dest='scanall',
			default=False,
			help='Scan all'),
		)
	args = ''
	help = 'Scan'

	def handle(self, *args, **options):
		concurrent                        = 15
		requests.adapters.DEFAULT_RETRIES = 5
		setting_timezone                  = timezone(settings.TIME_ZONE)

		def doWork():
			while True:
				try:
					company           =q.get()
					time_period       = '5Y'
					bulk              = []
					url               = 'http://www.bloomberg.com/apps/data?pid=webpxta&Securities=%s:TB&TimePeriod=%s&Outfields=HDATE,PR005-H,PR006-H,PR007-H,PR008-H,PR013-H' % (company.name, time_period)
					r                 = requests.get(url)
					data_line         = r.text.split('\n')[1:]
					lastday_quotation = None
					for data_list in data_line:
						data_list = data_list.split('"')
						if len(data_list) < 5:
							continue

						''' FORMAT: Date,Close,Open,High,Low,Volume '''
						naive_time     = dt.strptime(data_list[0], "%Y%m%d")
						create_on      = setting_timezone.localize(naive_time)
						create_date    = create_on.date()
						close_price    = float_or_zero(data_list[1])
						open_price     = float_or_zero(data_list[2])
						high_price     = float_or_zero(data_list[3])
						low_price      = float_or_zero(data_list[4])

						if close_price == float(0): # no data
							continue

						stock_quotation = Quotation.objects.filter(company=company,create_date=create_date).first()

						if stock_quotation:
							pass
						else:
							stock_quotation = Quotation(
								company     =company,
								close_price =close_price,
								open_price  =open_price,
								high_price  =high_price,
								low_price   =low_price,
								create_on   =create_on,
								create_date =create_date)
							bulk.append(stock_quotation)
					if len(bulk) > 0:
						query_q.put(bulk)
						# DayQuotation.objects.bulk_create(bulk)
						bulk = []
				except:
					q.task_done()
					raise
				q.task_done()

		def doInsert():
			while True:
				try:
					query_bulk = query_q.get()
					Quotation.objects.bulk_create(query_bulk)
					print 'insert %s' % (query_bulk[0].company.name)
				except:
					query_q.task_done()
					raise
				query_q.task_done()

		def float_or_zero(text):
			try:
				return float(text)
			except:
				return float(0)
		def int_or_zero(text):
			try:
				return int(text)
			except:
				return int(0)

		q       =Queue(concurrent*2)
		query_q = Queue()

		# DayQuotation.objects.all().delete() # delete all

		companies = Company.objects.all()
		print 'Scan all stock'

		for i in range(concurrent):
			t        =Thread(target=doWork)
			t.daemon =True
			t.start()
		try:
			for company in companies:
				q.put(company)
			q.join()
			
			t        =Thread(target=doInsert)
			t.daemon =True
			t.start()
			query_q.join()
		except KeyboardInterrupt:
			sys.exit(1)

			
		self.stdout.write('Successfully scan')