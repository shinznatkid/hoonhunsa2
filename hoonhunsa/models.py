# -*- coding: utf-8 -*-
from django.utils import timezone
from django.db import models
import calendar


class Company(models.Model):
	name = models.CharField(max_length=255)

	def __unicode__(self):
		return self.name

class Quotation(models.Model):
	company     = models.ForeignKey(Company, related_name='quotations')
	create_date = models.DateField()
	create_on   = models.DateTimeField()

	close_price   = models.FloatField()
	open_price  = models.FloatField()
	high_price  = models.FloatField()
	low_price   = models.FloatField()

	def __unicode__(self):
		return '%s : %s' % (self.company.name,self.create_date)

	@property
	def create_timestamp(self):
		timet = timezone.localtime(self.create_on).timetuple()
		return (int(calendar.timegm(timet)) * 1000) + int(self.create_on.microsecond/1000)
